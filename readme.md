## Classes

<dl>
<dt><a href="#Server_http">Server_http</a></dt>
<dd><p>(license MIT) simple http server, full example - see example.js</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#type_pair">type_pair</a></dt>
<dd></dd>
</dl>

<a name="Server_http"></a>

## Server\_http
(license MIT) simple http server, full example - see example.js

**Kind**: global class  

* [Server_http](#Server_http)
    * [.ip](#Server_http+ip)
    * [.port](#Server_http+port)
    * [.server](#Server_http+server)
    * [.emit](#Server_http+emit)
    * [.on](#Server_http+on)
    * [.start([ip], [port])](#Server_http+start)

<a name="Server_http+ip"></a>

### server_http.ip
{string}

**Kind**: instance property of [<code>Server\_http</code>](#Server_http)  
<a name="Server_http+port"></a>

### server_http.port
{number}

**Kind**: instance property of [<code>Server\_http</code>](#Server_http)  
<a name="Server_http+server"></a>

### server_http.server
{any}

**Kind**: instance property of [<code>Server\_http</code>](#Server_http)  
<a name="Server_http+emit"></a>

### server_http.emit
{any}

**Kind**: instance property of [<code>Server\_http</code>](#Server_http)  
<a name="Server_http+on"></a>

### server_http.on
{any}

**Kind**: instance property of [<code>Server\_http</code>](#Server_http)  
<a name="Server_http+start"></a>

### server_http.start([ip], [port])
start http server

**Kind**: instance method of [<code>Server\_http</code>](#Server_http)  

| Param | Type | Description |
| --- | --- | --- |
| [ip] | <code>string</code> | ip, default 127.0.0.1 |
| [port] | <code>number</code> | port, default 3000 |

<a name="type_pair"></a>

## type\_pair
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| variable | <code>string</code> | variable |
| [value] | <code>any</code> | value |

