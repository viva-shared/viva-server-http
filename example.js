let lib_vserv = require('viva-server-http')()

lib_vserv.on('listening', function(host) {
    console.log ('http server '+host+' started...')
})

// main event for answer on incoming message POST
lib_vserv.on('post', function(data, sender, incomingMessageHttp, serverResponseHttp) {
    //echo server
    sender(200, data)
})

// main event for answer on incoming message GET
lib_vserv.on('get', function(data, sender, incomingMessageHttp, serverResponseHttp) {
    //echo server
    sender(200, data)
})

// errors in lib_vserv.on(... sender(...) ...)
lib_vserv.on('error', function(error, sender, incomingMessageHttp, serverResponseHttp) {
    sender(500,error.toString())
})

try {
    lib_vserv.start('127.0.0.1',3000)
} catch (error) {
    console.log(error)
}

